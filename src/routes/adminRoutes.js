import AdminLayout from "../Layout/AdminLayout"
import AdminPage from "../AdminPage/AdminPage"
import NewProccess from "../New Proccess/NewProccess"
import AllUsers from "../NewAccount/AllUsers"
import HomePage from "../HomePage/HomePage"
import TimeLine from "../TimeLine/TimeLine"

export const adminRoutes = [
    {
        url: "/admin-page",
        component: <AdminLayout Component={AdminPage}/>
    },

    {
        url: "/home-page",
        component: <AdminLayout Component={HomePage}/>
    },

    {
        url: "/create",
        component: <AdminLayout Component={NewProccess}/>
    },

    {
        url: "/all-user",
        component: <AdminLayout Component={AllUsers}/>
    },

    {
        url: "/time-line",
        component: <AdminLayout Component={TimeLine}/>
    }
]