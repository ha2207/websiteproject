import { Space, Table, Tag } from 'antd';
const { Column, ColumnGroup } = Table;
const data = [
  {
    key: '1',
    firstName: 'Trần Nguyễn',
    lastName: 'Hồng Hà',
    age: 23,
    address: 'Ho Chi Minh City, District 9',
    tags: ['admin', 'frontend'],
  },
  {
    key: '2',
    firstName: 'Phạm Nguyễn',
    lastName: 'Ngọc Tuân',
    age: 24,
    address: 'Ho Chi Minh City, Long An',
    tags: ['tester', 'frontend'],
  },
  {
    key: '3',
    firstName: 'Cù',
    lastName: 'Duy Lộc',
    age: 24,
    address: 'Ho Chi Minh City',
    tags: ['tester', 'backend'],
  },
];
const AllUsers = () => (
  <Table dataSource={data}>
    <ColumnGroup title="Name">
      <Column title="First Name" dataIndex="firstName" key="firstName" />
      <Column title="Last Name" dataIndex="lastName" key="lastName" />
    </ColumnGroup>
    <Column title="Age" dataIndex="age" key="age" />
    <Column title="Address" dataIndex="address" key="address" />
    <Column
      title="Tags"
      dataIndex="tags"
      key="tags"
      render={(tags) => (
        <>
          {tags.map((tag) => (
            <Tag color="blue" key={tag}>
              {tag}
            </Tag>
          ))}
        </>
      )}
    />
    <Column
      title="Action"
      key="action"
      render={(_, record) => (
        <Space size="middle">
          <a>Invite {record.lastName}</a>
          <a>Delete</a>
        </Space>
      )}
    />
  </Table>
);
export default AllUsers;
