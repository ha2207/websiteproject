import { SET_USER_LOGIN } from "../constants/userConstant";

// const initialState = {
//   userInfo: localUserServ.get(),
// };

let userReducer = ({ type, payload }) => {
  switch (type) {
    case SET_USER_LOGIN: {
      return { userInfo: payload };
    }
    default:
      return ;
  }
};
export default userReducer;