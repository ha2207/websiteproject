import { userServ } from "../../services/userService";
import {SET_USER_LOGIN} from "../constants/userConstant"

export const setLoginAction = (values) => {
    return{
        //values den tu res cua api
        type: SET_USER_LOGIN,
        payload: values,
    }
  
}

export const setLoginActionService = (values, onSuccess) => {
    // values đến từ form của antd
    return (dispatch) => {
      userServ
        .login(values)
        .then((res) => {
          console.log(res);
          dispatch({
            type: SET_USER_LOGIN,
            payload: res,
          });
          onSuccess();
        })
        .catch((err) => {
          console.log(err);
        });
    };
  };
