import axios from "axios";

export const BASE_URL = "https://6423c25b77e7062b3e384c16.mockapi.io/"

export const https = axios.create({
    baseURL: BASE_URL,
})