import { Button, Form, Input, Select, message, Upload } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { InboxOutlined } from "@ant-design/icons";
import ImgCrop from 'antd-img-crop';

import React, { useState }  from "react";
import FormItem from "antd/es/form/FormItem";
import { Link } from "react-router-dom";

const { Dragger } = Upload;
const props = {
  name: "file",
  action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
  headers: {
    authorization: "authorization-text",
  },
  onChange(info) {
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};
const { Option } = Select;
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};
const NewProccess = () => {
  const formRef = React.useRef(null);
  const onApproveChange = (value) => {
    switch (value) {
      case "Private":
        formRef.current?.setFieldsValue({
          note: "Just only approver (role of this) can see this post",
        });
        break;
      case "Public":
        formRef.current?.setFieldsValue({
          note: "Every roles can see this post",
        });
        break;
      default:
        break;
    }
  };
  const onFinish = (values) => {
    console.log(values);
  };
  const onReset = () => {
    formRef.current?.resetFields();
  };
  const onFill = () => {
    formRef.current?.setFieldsValue({
      note: "Hello world!",
      gender: "male",
    });
  };
  const [fileList, setFileList] = useState([
    {
      uid: '-1',
      name: 'image.png',
      status: 'done',
      url: '../img/Indigo-Header.png',
    },
  ]);
  const onChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };
  const onPreview = async (file) => {
    let src = file.url;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };
  return (
    <Form
      {...layout}
      ref={formRef}
      name="control-ref"
      onFinish={onFinish}
      style={{
        maxWidth: 600,
      }}>
      <Form.Item
        name="note"
        label="Name Proccess"
        rules={[
          {
            required: true,
          },
        ]}>
        <Input />
      </Form.Item>
      <Form.Item
        name="approve"
        label="Approve"
        rules={[
          {
            required: true,
          },
        ]}>
        <Select
          placeholder="Select a option and change input text above"
          onChange={onApproveChange}
          allowClear>
          <Option value="Private">Private</Option>
          <Option value="Public">Public</Option>
        </Select>
      </Form.Item>

      <Form.Item
        noStyle
        shouldUpdate={(prevValues, currentValues) =>
          prevValues.approve !== currentValues.approve
        }>
        {({ getFieldValue }) =>
          getFieldValue("approve") === "other" ? (
            <Form.Item
              name="customizeApprove"
              label="Customize Approve"
              rules={[
                {
                  required: true,
                },
              ]}>
              <Input />
            </Form.Item>
          ) : null
        }
      </Form.Item>

      <FormItem {...tailLayout} >
        <Upload {...props} type="link" htmlType="button" onClick={onFill}>
          <Button icon={<UploadOutlined />}>Add File</Button>
        </Upload>
        <br/>
        <ImgCrop rotationSlider>
      <Upload
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        listType="picture-card"
        fileList={fileList}
        onChange={onChange}
        onPreview={onPreview}
      >
        {fileList.length < 5 && '+ Upload'}
      </Upload>
    </ImgCrop>
        <p>
          or
        </p>
        <br/>
        <Dragger {...props}>
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">
            Click or drag file to this area to upload
          </p>
          <p className="ant-upload-hint">
            Support for a single or bulk upload. Strictly prohibited from
            uploading company data or other banned files.
          </p>
        </Dragger>
      </FormItem>

      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
        <Button htmlType="button" onClick={onReset}>
          Reset
        </Button>
        <Button type="link" htmlType="button" onClick={onFill}>
          Fill form
        </Button>
        <Link to="../AdminPage/AdminPage.js">
          <Button type="primary">Back</Button>
        </Link>
      </Form.Item>
    </Form>
  );
};
export default NewProccess;
