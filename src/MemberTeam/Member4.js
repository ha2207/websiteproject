import React, { useState } from 'react';
// import { PlusOutlined } from '@ant-design/icons';
import { Button, Drawer, Select } from 'antd';

const { Option } = Select;

const Member4 = () => {
  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Button type="pink" onClick={showDrawer}>
      V.Mỹ Châu
      </Button>
      <Drawer
        title="Profile User"
        width={620}
        onClose={onClose}
        open={open}
        bodyStyle={{ paddingBottom: 80 }}
      >
      <h1><strong>Name</strong></h1>
      <p>Vương Mỹ Châu</p>
        <br></br>
      <h1><strong>Age</strong></h1>
      <p>23</p>
      <br></br>
      <h1><strong>BOD</strong></h1>
      <p>MeowMeow Duunt know, 2000</p>
      <br></br>
      <h1><strong>Email</strong></h1>
      <p>mychau101@gmail.com</p>
      <br></br>
      <h1><strong>Phone Number</strong></h1>
      <p>+84 700 258 365</p>
      <br></br>
      <h1><strong>Role</strong></h1>
      <p>Developer/Main BackEnd Progresser</p>
      </Drawer>
    </>
  );
};

export default Member4;