import React, { useState } from 'react';
// import { PlusOutlined } from '@ant-design/icons';
import { Button, Drawer, Select } from 'antd';

const { Option } = Select;

const Member2 = () => {
  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Button type="pink" onClick={showDrawer}>
      PN.Ngọc Tuân
      </Button>
      <Drawer
        title="Profile User"
        width={620}
        onClose={onClose}
        open={open}
        bodyStyle={{ paddingBottom: 80 }}
      >
      <h1><strong>Name</strong></h1>
      <p>Phạm Nguyễn Ngọc Tuân</p>
        <br></br>
      <h1><strong>Age</strong></h1>
      <p>24</p>
      <br></br>
      <h1><strong>BOD</strong></h1>
      <p>March 09, 1999</p>
      <br></br>
      <h1><strong>Email</strong></h1>
      <p>ngoctuan456@gmail.com</p>
      <br></br>
      <h1><strong>Phone Number</strong></h1>
      <p>+84 123 263 789</p>
      <br></br>
      <h1><strong>Role</strong></h1>
      <p>Tester/ FrontEnd Progresser</p>
      </Drawer>
    </>
  );
};

export default Member2;