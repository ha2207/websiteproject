import React, { useState } from 'react';
// import { PlusOutlined } from '@ant-design/icons';
import { Button, Drawer, Select } from 'antd';

const { Option } = Select;

const Member3 = () => {
  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Button type="pink" onClick={showDrawer}>
      C.Duy Lộc
      </Button>
      <Drawer
        title="Profile User"
        width={620}
        onClose={onClose}
        open={open}
        bodyStyle={{ paddingBottom: 80 }}
      >
      <h1><strong>Name</strong></h1>
      <p>Cù Duy Lộc</p>
        <br></br>
      <h1><strong>Age</strong></h1>
      <p>24</p>
      <br></br>
      <h1><strong>BOD</strong></h1>
      <p> I dunnt know, 1999</p>
      <br></br>
      <h1><strong>Email</strong></h1>
      <p>duyloc789@gmail.com</p>
      <br></br>
      <h1><strong>Phone Number</strong></h1>
      <p>+84 038 263 123</p>
      <br></br>
      <h1><strong>Role</strong></h1>
      <p>Tester/ Backend Progresser</p>
      </Drawer>
    </>
  );
};

export default Member3;