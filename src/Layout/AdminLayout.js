import {
  FileOutlined,
  FundProjectionScreenOutlined,
  FieldTimeOutlined,
  HomeOutlined,
  PlusOutlined,
  UserOutlined,
  TeamOutlined,
  SafetyCertificateOutlined,
  HeartOutlined,
  LogoutOutlined,
  UnorderedListOutlined,
} from "@ant-design/icons";
import { Breadcrumb, Layout, Menu, theme } from "antd";
import { Component, useState } from "react";
import NewAccount from "../NewAccount/NewAccount";
import Member1 from "../MemberTeam/Member1";
import Member2 from "../MemberTeam/Member2";
import Member3 from "../MemberTeam/Member3";
import Member4 from "../MemberTeam/Member4";
import Member5 from "../MemberTeam/Member5";
import Member6 from "../MemberTeam/Member6";
import { NavLink } from "react-router-dom";
//import AllUsers from "../NewAccount/AllUsers";
//import TimeLine from "../TimeLine/TimeLine";
// import SecurityandPolicy from "../SecurityandPolicy/SecurityandPolicy";
const { Header, Content, Footer, Sider } = Layout;
function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}
const items = [
  getItem("Home", "1", <HomeOutlined />),
  // getItem("Personal Information", "2", <DesktopOutlined />),
  getItem("Proccess", "sub1", <FundProjectionScreenOutlined />, [
    getItem(
      <NavLink to={"/create"}>Create New Proccess</NavLink>,
      "4",
      <PlusOutlined />
    ),
    getItem("Newest", "5"),
    getItem("Lastest", "6"),
    getItem("Important", "7"),
  ]),
  getItem( "Account", "8", <UserOutlined />,[
    getItem(<NewAccount />, "New Account", <PlusOutlined />),
    getItem(<NavLink to={"/all-user"}>All Users</NavLink>
    ,"9", <UnorderedListOutlined />),
  ]),
  getItem("Team", "sub3", <TeamOutlined />, [
    getItem(<Member1 />, "Trần Nguyễn Hồng Hà", <UserOutlined />),
    getItem(<Member2 />, "PN.Ngọc Tuân", <UserOutlined />),
    getItem(<Member3 />, "C.Duy Lộc", <UserOutlined />),
    getItem(<Member4 />, "V.Mỹ Châu", <UserOutlined />),
    getItem(<Member5 />, "V.Việt Nam", <UserOutlined />),
    getItem(<Member6 />, "NT.Xuân An", <UserOutlined />),
  ]),
  getItem("Files Stogares", "12", <FileOutlined />),
  getItem(<NavLink to={"/time-line"}>TimeLine</NavLink>,"13", <FieldTimeOutlined />),
  getItem("About Us", "14", <HeartOutlined />),
  getItem("Security", "15", <SafetyCertificateOutlined />),
  getItem("Log Out", "16", <LogoutOutlined />),
];
const AdminLayout = ({ Component }) => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout
      style={{
        minHeight: "100vh",
      }}>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}>
        <div
          style={{
            height: 32,
            margin: 16,
            background: "rgba(255, 255, 255, 0.2)",
          }}
        />
        <Menu
          className=" bg-blue-300 text-blue-950"
          theme="light"
          defaultSelectedKeys={["1"]}
          mode="inline"
          items={items}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        />
        <Content
          style={{
            margin: "0 16px",
          }}>
          <div
            style={{
              padding: 24,
              minHeight: 360,
              background: colorBgContainer,
            }}>
            <Component />
          </div>
        </Content>
        <Footer
          style={{
            textAlign: "center",
          }}>
          MeowMeow ©2023 Created by MeowMeow UniCom
        </Footer>
      </Layout>
    </Layout>
  );
};
export default AdminLayout;
