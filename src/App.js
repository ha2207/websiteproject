//import logo from "./logo.svg";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
//import Login from "./LoginPage/Login";
import LoginPage from "./LoginPage/LoginPage";
//import Member1 from "./MemberTeam/Member1";
import { adminRoutes } from "./routes/adminRoutes";
// import NewAccount from "./NewAccount/NewAccount"

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {adminRoutes.map(({ url, component }) => {
            return <Route path={url} element={component} />;
          })}
          <Route path="/login" element={<LoginPage/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
