import React, { useState } from 'react';
// import { PlusOutlined } from '@ant-design/icons';
import { Button, Drawer, Select } from 'antd';

const { Option } = Select;

const SecurityandPolicy = () => {
  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Button type="pink" onClick={showDrawer}>
      Security & Policy
      </Button>
    <div 
        onClose={onClose}
        open={open}
        bodyStyle={{ paddingBottom: 80 }}>
      <h1><strong>Information Security</strong></h1>
      <h2>Information Security is everybody’s responsibility.</h2>
      <hr>
      </hr>
      <p>Whether you're a student or a member of staff, it is everybody's responsibility to keep University data and IT systems secure. 
      Find out how to protect yourself, your data and your devices and access the training, policies and procedures we use to keep our 
      data safe.</p>
    </div>
  </>
  );
};

export default SecurityandPolicy;
